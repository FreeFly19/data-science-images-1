from sklearn.datasets import load_digits
import cv2
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score


digits = load_digits()

X = digits.data / 16 * 255
y = digits.target

cv2.imwrite('example.png', X[0].reshape((8,8)))

model = DecisionTreeClassifier(random_state=45)
model.fit(X[:1200], y[:1200])

y_pred = model.predict(X[1200:])

print(accuracy_score(y[1200:], y_pred))

# import matplotlib.pyplot as plt
# from sklearn.metrics import ConfusionMatrixDisplay
# ConfusionMatrixDisplay.from_predictions(y[1200:], y_pred)
# plt.show()

img = cv2.imread('5.png')
img = cv2.resize(img[:,:,0], (8,8), cv2.INTER_CUBIC)
cv2.imwrite('5_resized.png', img)
print(img)

print(model.predict(img.reshape(1, 64)))
